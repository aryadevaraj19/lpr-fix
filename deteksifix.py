
import cv2


from ultralytics import YOLO
from ultralytics.utils.plotting import Annotator
import logging
import  time
import numpy as np
import os



import cv2
import matplotlib.pyplot as plt
import numpy as np
import os


from scipy import ndimage

def shi_tomashi(image):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    corners = cv2.goodFeaturesToTrack(gray, 27, 0.01, 70)
    corners = np.int0(corners)
    corners = sorted(np.concatenate(corners).tolist())
    print('\nThe corner points are...\n')

    im = image.copy()
    for index, c in enumerate(corners):
        x, y = c
        cv2.circle(im, (x, y), 3, 255, -1)
        character = chr(65 + index)
        print(character, ':', c)
        cv2.putText(im, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2, cv2.LINE_AA)

    # plt.imshow(im)
    # plt.title('Corner Detection: Shi-Tomashi')
    # plt.show()
    return corners


def get_destination_points(corners):
    w1 = np.sqrt((corners[0][0] - corners[1][0]) ** 2 + (corners[0][1] - corners[1][1]) ** 2)
    w2 = np.sqrt((corners[2][0] - corners[3][0]) ** 2 + (corners[2][1] - corners[3][1]) ** 2)
    w = max(int(w1), int(w2))

    h1 = np.sqrt((corners[0][0] - corners[2][0]) ** 2 + (corners[0][1] - corners[2][1]) ** 2)
    h2 = np.sqrt((corners[1][0] - corners[3][0]) ** 2 + (corners[1][1] - corners[3][1]) ** 2)
    h = max(int(h1), int(h2))

    destination_corners = np.float32([(0, 0), (w - 1, 0), (0, h - 1), (w - 1, h - 1)])

    print('\nThe destination points are: \n')
    for index, c in enumerate(destination_corners):
        character = chr(65 + index) + "'"
        print(character, ':', c)

    print('\nThe approximated height and width of the original image is: \n', (h, w))
    return destination_corners, h, w

def unwarp(img, src, dst):
    # cv2_imshow(img)
    h, w = img.shape[:2]
    src_coords = []

    for x in src:
      src_coords.append([x[1],x[0]])
      print("x => ",x[0]," - ",x[1])

    # print("src")
    # print(src)
    # src_coords = src
    src_image = img.copy()
    src_pts = np.float32(src_coords).reshape(-1,1,2)
    print(src_pts)
    dst_pts = np.float32([[0,0],[w,0],[w,h],[0,h]]).reshape(-1,1,2)
    matrix = cv2.getPerspectiveTransform(src_pts, dst_pts)
    dst_image = cv2.warpPerspective(src_image,matrix,(w,h))
    # cv2_imshow(dst_image)


    H, _ = cv2.findHomography(src, dst, method=cv2.RANSAC, ransacReprojThreshold=1.0)
    print('\nThe homography matrix is: \n', H)
    un_warped = cv2.warpPerspective(img, H, (h, w), flags=cv2.INTER_LINEAR)
    # plot

    # f, (ax1, ax2) = plt.subplots(1, 2)
    # ax1.imshow(img)
    # x = [src[0][0], src[2][0], src[3][0], src[1][0], src[0][0]]
    # y = [src[0][1], src[2][1], src[3][1], src[1][1], src[0][1]]
    # ax1.plot(x, y, color='yellow', linewidth=3)
    # ax1.set_ylim([h, 0])
    # ax1.set_xlim([0, w])
    # ax1.set_title('Targeted Area in Original Image')
    # ax2.imshow(un_warped)
    # ax2.set_title('Unwarped Image')
    # plt.show()

    # cv2_imshow(un_warped)
    return un_warped

def apply_threshold(filtered):
    ret, thresh = cv2.threshold(filtered, 250, 255, cv2.THRESH_OTSU)
    return thresh

def detect_contour(img, image_shape):
    canvas = np.zeros(image_shape, np.uint8)
    contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    cnt = sorted(contours, key=cv2.contourArea, reverse=True)[0]
    cv2.drawContours(canvas, cnt, -1, (0, 255, 255), 3)
    # plt.title('Largest Contour')
    # plt.imshow(canvas)
    # plt.show()

    return canvas, cnt


def apply_filter(image):
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    # cv2_imshow(image)
    kernel = np.ones((3, 3), np.float32) / 15
    filtered = cv2.filter2D(gray, -1, kernel)
    # plt.imshow(cv2.cvtColor(filtered, cv2.COLOR_BGR2RGB))
    # plt.title('Filtered Image')
    # plt.show()
    return filtered

def detect_corners_from_contour(canvas, cnt):
    epsilon = 0.02 * cv2.arcLength(cnt, True)
    approx_corners = cv2.approxPolyDP(cnt, epsilon, True)
    cv2.drawContours(canvas, approx_corners, -1, (255, 255, 0), 10)
    approx_corners = sorted(np.concatenate(approx_corners).tolist())
    print('\nThe corner points are ...\n')
    for index, c in enumerate(approx_corners):
        character = chr(65 + index)
        print(character, ':', c)
        cv2.putText(canvas, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    # Rearranging the order of the corner points
    approx_corners = [approx_corners[i] for i in [0, 1, 2, 3]]

    # plt.imshow(canvas)
    # plt.title('Corner Points')
    # plt.show()
    return approx_corners

def perspectiveCorrection(img):
    print("===== MAUSK KE perspectiveCorrection")
    # cv2_imshow(img)
    image = img.copy()
    filter_image = apply_filter(image)
    threshold_image = apply_threshold(filter_image)
    cnv, largest_contour = detect_contour(threshold_image, image.shape)
    corners = detect_corners_from_contour(cnv, largest_contour)
    destination_points, h, w = get_destination_points(corners)
    un_warped = unwarp(image, np.float32(corners), destination_points)

    # Cropped Image Perspective
    cropped = un_warped[1:h, 1:w]
    filtered_image_crop = apply_filter(cropped)
    threshold_image_crop = apply_threshold(filtered_image_crop)

    return cropped

def variance_of_laplacian(image):
	return cv2.Laplacian(image, cv2.CV_64F).var()


os.environ["OPENCV_FFMPEG_CAPTURE_OPTIONS"] = "rtsp_transport;udp"


# camera1 = cv2.VideoCapture('rtsp://admin:@192.168.1.18:2300/live',cv2.CAP_FFMPEG)  # use 0 for web camera
# camera2 = cv2.VideoCapture('rtsp://admin:@192.168.1.6:2300/live',cv2.CAP_FFMPEG)
# camera3 = cv2.VideoCapture('rtsp://admin:@192.168.1.3:2300/live',cv2.CAP_FFMPEG)
# camera1 = cv2.VideoCapture('video.mp4')


# model untuk mendeteksi lokasi plat 
model = YOLO('best_yolo_8.pt')
names = model.names


#engin ocr
#fungsi membaca huruf dan angka dari plat
from paddleocr import PaddleOCR,draw_ocr
ocr = PaddleOCR(use_angle_cls=True, lang='en') # need to run only once to download and load model into memory

# Create a VideoCapture object and read from input file 
# cap = cv2.VideoCapture('rtsp://192.168.18.44:8080/h264_ulaw.sdp')  # jika menggunakan ip cam  cctv
cap = cv2.VideoCapture('cctv_2.mp4') # jika menggunakan video
# cap = cv2.VideoCapture(0)   # jika menggunakan webcam laptop
count = 2000
# Check if camera opened successfully 
if (cap.isOpened()== False): 
    print("Error opening video file") 
  
# Read until video is completed 
while(cap.isOpened()): 
      
# Capture frame-by-frame 
    ret, frame = cap.read() 
    if ret == True: 
    # Display the resulting frame 
        frame_cp = frame.copy()

        #untuk medeteksi lokasi plat
        results = model(frame, verbose=False)

        # print("A----")
        # # print(results.pandas().xyxy[0])
        # print("----A")

        #hasil dari pendeteksian plat
        for r in results:
            boxes = r.boxes
            for box in boxes:
                b = box.cpu().xyxy[0].numpy()
                c = box.cls


                #hasil koordinat plat x,y
                x1 = int(b[0]) -5
                y1 = int(b[1]) -5
                x2 = int(b[2]) +5
                y2 = int(b[3]) +5



                #mendapatkan hasil prediksi plat/mobil
                hasil_deteksinya = model.names[int(c)]

                #akurasi pendeteksian plat 
                cp = int(round(float(str(box.cpu().conf[0].numpy())),2) *100)

                fm = 0

                #kondisi jika plat dan akurasinya diatas 70%
                if hasil_deteksinya == 'plat' and cp > 70:
                  
                  #mendeteksi apakah platnya blur atau tidak
                  fm = variance_of_laplacian(cv2.cvtColor(frame_cp[y1:y2,x1:x2], cv2.COLOR_BGR2GRAY))


                  #jika nilai fm kurang 500, maka dianggap blur

                  
                
                  if fm > 500:
                    count = count+1
                    fm = 0

                    #crop image untuk lokasi platnya
                    w,h,z = frame_cp[y1:y2,x1:x2].shape
                    new_image = cv2.resize(frame_cp[y1:y2,x1:x2], (h,w), interpolation=cv2.INTER_NEAREST)

                    try:
                        #mendeteksi huruf dan angka pada plat menggunakan engine paddle ocr
                        result = ocr.ocr(new_image, cls=False,bin=False)

                        #ambil hasil ocr 
                        txts = [line[0][1] for line in result]


                        #str(txts[0][0]) --> hasil huruf dan plat
                        # str(round(txts[0][1])) --> hasil akurasi huruf dan plat


                        print(str(txts[0][0])+" == "+str(round(txts[0][1])))


                        #


                        if int(round((txts[0][1] *100)) > 60):
                            #tampilkan pada gambar hasil plat dan hurufnya
                            hasil_plat_huruf = "PLAT "+str(txts[0][0])+" : "+str(int(round((txts[0][1] *100))))+"%"
                            text_size, _ = cv2.getTextSize(hasil_plat_huruf, cv2.FONT_HERSHEY_TRIPLEX, 1, 2)
                            text_w, text_h = text_size
                            cv2.rectangle(frame_cp, (x1,y1-20), (x1 + text_w, y1-20 + text_h), (0, 0, 255), -1)
                            cv2.putText(frame_cp, hasil_plat_huruf, (x1, y1-20 + text_h + 1 - 1), cv2.FONT_HERSHEY_TRIPLEX, 1, (255, 255,255), 2)

                    except :
                        pass

                # if hasil_deteksinya != '' and hasil_deteksinya != 'plat':
                #     text_size, _ = cv2.getTextSize(hasil_deteksinya, cv2.FONT_HERSHEY_TRIPLEX, 1, 2)
                #     text_w, text_h = text_size
                #     cv2.rectangle(frame_cp, (x1,y1-40), (x1 + text_w, y1-40 + text_h), (0, 0, 255), -1)
                #     cv2.putText(frame_cp, hasil_deteksinya, (x1, y1-40 + text_h + 1 - 1), cv2.FONT_HERSHEY_TRIPLEX, 1, (255, 255,255), 2)

                    
                    continue
        cv2.imshow('Frame', frame_cp) 
          
    # Press Q on keyboard to exit 
        if cv2.waitKey(25) & 0xFF == ord('q'): 
            break
  
# Break the loop 
    else: 
        break
  
# When everything done, release 
# the video capture object 
cap.release() 
  
# Closes all the frames 
cv2.destroyAllWindows() 